import json
import logging
import os

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

'''This function loads the custom parameters into environment variables.'''
def load_custom_parameters():
    parametersObject = json.loads(os.environ['CUSTOM_PARAMETERS'])
    for key, value in parametersObject.items():
        os.environ[key] = value

class Predictor():
    def load(self):
        # TODO perform all one-time steps needed at startup here (e.g. loading the persistent model)
        pass

    def predict(self, raw_data_sample):
        # TODO replace with your own prediction function
        raw_data_sample = json.loads(raw_data_sample)
        logger.debug(f"Predicting on {raw_data_sample}")
        if raw_data_sample['data1'] == 'giraffe':
            return json.dumps({'prediction1': 1.0})
        else:
            return json.dumps({'prediction1': 0.0})


def main():
    load_custom_parameters()
    predictor = Predictor()
    predictor.load()
    result = predictor.predict({'feature1': 1, 'feature2': 2})
    # print all env variables here:
    print(os.environ)


if __name__ == "__main__":
    main()
